<?php

Route::group(['middleware' => ['locale']], function () {

	Route::get('', 'LoginController@getLogin');
	Route::post('login', 'LoginController@postLogin');
	Route::any('logout', 'LoginController@getLogout');
	Route::any('local/{lang}', 'LocalizationController@setLocale');

	//Защита админки посредником через роль аккаунта из БД (админ - 1, юзер - 2)
	Route::group(['middleware' => ['auth.admin'], 'prefix' => 'manage'], function () {

		Route::any("/main", "Manage\ManageController@getIndex");
		Route::get('/org_structure',array('as'=>'jquery.treeview','uses'=>'OrgStructureController@treeView'));
		Route::get("/org_structure/edit/{id}", "OrgStructureController@edit_row");

		Route::get("/all_clients", "Manage\ManageController@all_clients");
		Route::get("/edit_user/{id}", "Manage\ManageController@edit_user");
		Route::post("/save_edited_user", "Manage\ManageController@save_edited_user");
		Route::get("/add_client", "Manage\ManageController@add_client");
		Route::get("/delete_user/{id}", "Manage\ManageController@del_user");
		Route::post('/add_client_post', 'Manage\ManageController@addClientPOST');

	});

	//Для обычного юзера роуты через простой посредник, проверяющий только авторизацию
	Route::group(['middleware' => ['auth.user'], 'prefix' => 'user'], function () {
		
	});

});