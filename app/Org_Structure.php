<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Org_structure extends Model
{
   protected $table = "org_structure";
   //category has childs
   public function childs() {
           return $this->hasMany('App\Org_structure','parent_id','id') ;
   }
}