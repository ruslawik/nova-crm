<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Client;

class ApiController extends Controller
{

	public function AddNewClient (Request $r){

		if($r->input("hash") == "fa114668865cd930445f9c27f2da678908"){

			$new_client = new Client();

			$r->has('name') ? $new_client->name = $r->input('name') : "";
			$r->has('surname') ? $new_client->surname = $r->input('surname') : "";
			$r->has('city') ? $new_client->city = $r->input('city') : "";
			$r->has('tel_number') ? $new_client->tel_number = $r->input('tel_number') : "";
			$r->has('email') ? $new_client->email = $r->input('email') : "";
			$r->has('comment') ? $new_client->comment = $r->input('comment') : "";
			$new_client->hash = Hash::make($r->input('name').$r->input('surname').$r->input('tel_number').rand(111111,99999999));
			$new_client->school = $r->input('school');

			$saved = $new_client->save();

			if($saved){
				return response()->json([
	    			'response' => 'success',
	    			'number' => $new_client->id,
	    			'hash' => $new_client->hash
				]);
			}else{
				return response()->json([
	    			'response' => 'db_save_error',
				]);
			}
		}else{
			return response()->json([
	    			'response' => 'access_error',
				]);
		}
	}

}
