<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Application;
use Hash;
use Auth;

class LocalizationController extends Controller{
    function setLocale ($lang){

        $user = User::find(Auth::id());
        $user->locale = $lang;
        $user->save();

        return redirect()->back();
    }
}
