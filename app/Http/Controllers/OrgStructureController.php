<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Org_Structure;

class OrgStructureController extends Controller {

   public function treeView(){

        $ar['tree'] = $this->build_tree();
        $ar['title'] = trans('messages.title_org_structure');

        return view('manage.org_structure', $ar);
    }

    public function edit_row ($id){
        $Row = Org_Structure::where('id', '=', $id)->get()->toArray();
        $ar['tree'] = $this->build_tree();
        $ar['title'] = trans('messages.title_org_structure');
        $ar['edit'] = true;
        $ar['row'] = $Row;

        return view('manage.org_structure', $ar);
    } 

    public function build_tree (){
        $Categories = Org_Structure::where('parent_id', '=', 0)->get();
        $tree='<ul id="browser" class="filetree"><li class="tree-view"></li>';
        foreach ($Categories as $Category) {
             $tree .='<li class="tree-view open"<a class="tree-name">'.$Category->name.'</a> <a href="/manage/org_structure/edit/'.$Category->id.'"><i class="fa fa-edit"></i></a>';
             if(count($Category->childs)) {
                $tree .=$this->childView($Category);
            }
        }
        $tree .='</ul>';
        return $tree;
    }

    public function childView($Category){                 
            $html ='<ul>';
            foreach ($Category->childs as $arr) {
                if(count($arr->childs)){
                    $html .='<li class="tree-view open"><a class="tree-name">'.$arr->name.'</a> <a href="/manage/org_structure/edit/'.$arr->id.'"><i class="fa fa-edit"></i></a>';                  
                        $html.= $this->childView($arr);
                    }else{
                        $html .='<li class="tree-view"><a class="tree-name">'.$arr->name.'</a> <a href="/manage/org_structure/edit/'.$arr->id.'"><i class="fa fa-edit"></i></a>';                                 
                        $html .="</li>";
                    }
                                   
            }
            
            $html .="</ul>";
            return $html;
    }

}