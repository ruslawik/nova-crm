<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Application;
use App\Client;
use Hash;
use Auth;

class ManageController extends Controller{
    function getIndex (){
        $ar = array();
        $ar['title'] = "NOVA CRM System";
        $ar['username'] = Auth::user()->name;

        return view('manage.main', $ar);
    }

    public function all_clients (){
    	$ar = array();
        $ar['title'] = "NOVA All Clients";
        $ar['users'] = Client::orderBy('id', 'desc')->get();

    	return view('manage.all_clients', $ar);
    }

    public function edit_user ($id){
    	$ar = array();
    	$ar['title'] = "Edit client info";
    	$ar['user'] = Client::where("id", $id)->get();
    	$ar['action'] = action("Manage\ManageController@save_edited_user");

    	return view('manage.edit_user', $ar);
    }

    public function save_edited_user (Request $r){

    	$user_id = $r->input('user_id');
    	$name = $r->input('name');
    	$surname = $r->input('surname');
    	$city = $r->input('city');
    	$tel_number = $r->input('tel_number');
    	$comment = $r->input('comment');
    	$email = $r->input('email');

    	Client::where("id", $user_id)->update(["name"=>$name, "surname"=>$surname, "city"=>$city, "tel_number"=>$tel_number, "comment" => $comment, "email" => $email]);

    	return back();
    }

    public function add_client (){
    	$ar = array();
        $ar['title'] = "Add Client";
        $ar['action'] = action("Manage\ManageController@addClientPOST");

    	return view('manage.add_client', $ar);
    }

    public function addClientPOST (Request $r){

    	$new_client = new Client();

    	$r->has('name') ? $new_client->name = $r->input('name') : "";
		$r->has('surname') ? $new_client->surname = $r->input('surname') : "";
		$r->has('city') ? $new_client->city = $r->input('city') : "";
		$r->has('tel_number') ? $new_client->tel_number = $r->input('tel_number') : "";
		$r->has('email') ? $new_client->email = $r->input('email') : "";
		$r->has('comment') ? $new_client->comment = $r->input('comment') : "";

		$saved = $new_client->save();

    	return redirect('/manage/all_clients');
    }

    public function del_user ($id){

        Client::where('id', $id)->delete();

        return redirect("/manage/all_clients");
    }


}
