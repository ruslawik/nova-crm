@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Ред.</span> Edit user {{ $user['0']->name }} {{ $user['0']->surname }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Edit</strong>
                    <a style="float:right;" href="/manage/delete_user/{{$user['0']->id}}"><button class="btn btn-danger">DELETE</button></a>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ $action }}">
                    {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{ $user['0']->id }}">
                        <div class="input-group">
                                <div class="input-group-addon">Surname</div>
                                <input type="text" class="form-control col-sm-5" name="surname" value="{{ $user['0']->surname }}">
                        </div><br>
                        <div class="input-group">
                                <div class="input-group-addon">Name</div>
                                <input type="text" class="form-control col-sm-5" name="name" value="{{ $user['0']->name }}">
                        </div><br>
                        <div class="input-group">
                                <div class="input-group-addon">City</div>
                                <input type="text" class="form-control col-sm-5" name="city" value="{{ $user['0']->city }}">
                        </div><br>
                        <div class="input-group">
                                <div class="input-group-addon">Email</div>
                                <input type="text" class="form-control col-sm-5" name="email" value="{{ $user['0']->email }}">
                        </div><br>
                        <div class="input-group">
                                <div class="input-group-addon">Tel. number</div>
                                <input type="text" class="form-control col-sm-5" name="tel_number" value="{{ $user['0']->tel_number }}">
                        </div><br>
                        <div class="input-group">
                                <div class="input-group-addon">Comment</div>
                                <textarea class="form-control" name="comment">{{$user['0']->comment}}</textarea>
                        </div><br>
                        <input type="submit" value="Сохранить" class="btn btn-success">
                        <br>
                    </form>
                </div>
            </div>
    </div>



@endsection