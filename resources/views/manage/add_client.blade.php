@extends('manage.layout')

@section('title', $title)

@section('content')

    <div class="col-lg-6">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Add Client To CRM</strong>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="input-group">
                                            <div class="input-group-addon">Name</div>
                                            <input type="text" class="form-control" name="name">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Surname</div>
                                            <input type="text" class="form-control" name="surname">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Tel. number</div>
                                            <input type="text" class="form-control" name="tel_number">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">City</div>
                                            <select name="city" class="form-control">
                                                <option value="Almaty">Almaty</option>
                                                <option value="Astana">Astana</option>
                                                <option value="Atyrau">Atyrau</option>
                                                <option value="Shymkent">Shymkent</option>
                                            </select>
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Email</div>
                                            <input type="text" class="form-control" name="email">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Comment</div>
                                            <textarea class="form-control" name="comment"></textarea>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Добавить!</button>
                        </div>
                    </div>
            </form>
    </div>



@endsection