@extends('manage.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">All clients</span> @lang('messages.manage_all_clients')
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
    </div>

    <div class="col-lg-12">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                      	<th>Uniq. ID</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Город</th>
                        <th>Уч. заведение</th>
                        <th>Телефон</th>
                        <th>Email</th>
                        <th>Комментарий</th>
                        <th>Дата</th>
                        <th>Ред.</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>cl{{$user->id}}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->surname }}</td>
                                <td>{{ $user->city }}</td>
                                <td>{{ $user->school }}</td>
                                <td>{{ $user->tel_number }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->comment }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td><center><a href="/manage/edit_user/{{ $user->id }}"><i class="fa fa-pencil"></i></a></center></td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
    </div>



@endsection

@section('datatable_js')
	<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {
          jQuery('#bootstrap-data-table-export').DataTable();
        } );
    </script>

@endsection