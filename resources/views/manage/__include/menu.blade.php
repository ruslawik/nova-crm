<ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/manage/main"> <i class="menu-icon fa fa-dashboard"></i>@lang('messages.menu_main_page') </a>
                    </li>
                    <!--
                    <h3 class="menu-title">@lang('messages.menu_business_processes')</h3>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>@lang('messages.menu_tasks')</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-list"></i><a href="/manage/all_tasks">@lang('messages.menu_all_tasks')</a></li>
                            <li><i class="fa fa-pencil"></i><a href="/manage/add_task">@lang('messages.menu_add_a_task')</a></li>
                        </ul>
                    </li>
                    !-->
                    <h3 class="menu-title">@lang('messages.menu_clients')</h3>
                        <li><a href="/manage/all_clients"><i class="menu-icon fa fa-list"></i>@lang('messages.menu_all_clients')</a></li>
                        <li><a href="/manage/add_client"><i class="menu-icon fa fa-puzzle-piece"></i>@lang('messages.menu_add_a_client')</a></li>
                    <!--
                    <h3 class="menu-title">@lang('messages.menu_know_base')</h3>
                        <li><a href="/manage/info"><i class="menu-icon fa fa-list"></i>@lang('messages.menu_info')</a></li>
                        <li><a href="/manage/org_structure"><i class="menu-icon fa fa-building"></i>@lang('messages.menu_org_structure')</a></li>
                        <li><a href="/manage/add_info"><i class="menu-icon fa fa-pencil"></i>@lang('messages.menu_add_a_know_base_row')</a></li>
                    !-->
                    <h3 class="menu-title">@lang('messages.menu_finances')</h3>
                        <li><a href="/manage/finance_outcome"><i class="menu-icon fa fa-credit-card"></i>@lang('messages.menu_finance_outcome')</a></li>
                        <li><a href="/manage/finance_income"><i class="menu-icon fa fa-money"></i>@lang('messages.menu_finance_income')</a></li>
                        <li><a href="/manage/finance_analyse"><i class="menu-icon fa fa-signal"></i>@lang('messages.menu_finance_analytics')</a></li>

                </ul>