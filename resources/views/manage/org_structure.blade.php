@extends('manage.layout')

@section('title', $title)

@section('org_structure_css')
    
    <link rel="stylesheet" href="/manage_res/assets/css/treeview.css" />

@endsection

@section('content')

    <div class="col-sm-6">
            <div class="container">      
                {!! $tree !!}
            </div> 
    </div>

    @if (isset($edit))
        <div class="col-sm-6">
            {{ $row['0']['name'] }}
        </div>
    @endif

@endsection

@section('org_structure_js')
    <script src="/manage_res/js/treeview.js"></script>
    <script type="text/javascript" src="/manage_res/js/treeview_start.js"></script>

@endsection
